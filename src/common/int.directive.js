(()=> {
  'use strict';
  angular
    .module('HETest')
    .directive('int', function () {

      return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
          ctrl.$validators.integer = function(modelValue, viewValue) {
            if (ctrl.$isEmpty(modelValue)) {
              return true;
            }
            return /^\+?(0|[1-9]\d*)$/.test(viewValue);
          };
        }
      };
    });
})();