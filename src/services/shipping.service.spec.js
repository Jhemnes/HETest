describe('Shipping service', function() {
  var shippingService;

  beforeEach(module('HETest'));
  beforeEach(inject(function (_shippingService_) {
    shippingService = _shippingService_;
    expect(shippingService).toBeDefined();
  }));

  it('should have three shipping options', function(){
    expect(shippingService.shippingOptions.length).toEqual(3);
  });

  it('should have a price and name for all shipping options', function(){
    shippingService.shippingOptions.forEach(function(shippingOption){
      expect(shippingOption.price).toBeDefined();
      expect(shippingOption.name).toBeDefined();
    })
  });

  it('should retain the selected shipping option', function(){
    shippingService.selectedOption = shippingService.shippingOptions[1];
    expect(shippingService.selectedOption).toBe(shippingService.shippingOptions[1]);
  });

});