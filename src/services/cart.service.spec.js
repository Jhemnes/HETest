describe('Cart Service', function () {
  var cartService;
  var shippingService;

  var mockProduct1 = {
    id: 932036,
    name: "Walkman Car Connecting Pack for MD Walkman and CD Walkman (Model# CPA-9C)",
    price: 14.85,
    image: {
      small: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=100&odnWidth=100&odnBg=ffffff",
      medium: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=180&odnWidth=180&odnBg=ffffff",
      large: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=450&odnWidth=450&odnBg=ffffff"
    },
    shortDescription: "Listen to your favorite music while driving with the Sony CPA-9C CD To Cassette Adapter. This product is shaped like a cassette and is made to fit inside a tape deck. The Minidisc adapter is an ideal companion for daily commutes and road trips.",
    rating: 2.333
  };

  var mockProduct2 = {
    id: 876589,
    name: "SONY Television",
    price: 15.37,
    image: {
      small: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=100&odnWidth=100&odnBg=ffffff",
      medium: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=180&odnWidth=180&odnBg=ffffff",
      large: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=450&odnWidth=450&odnBg=ffffff"
    },
    shortDescription: "Listen to your favorite music while driving with the Sony CPA-9C CD To Cassette Adapter. This product is shaped like a cassette and is made to fit inside a tape deck. The Minidisc adapter is an ideal companion for daily commutes and road trips.",
    rating: 4.2
  };

  beforeEach(module('HETest'));
  beforeEach(inject(function (_cartService_, _shippingService_) {
    cartService = _cartService_;
    shippingService = _shippingService_;

    expect(cartService).toBeDefined();
    expect(shippingService).toBeDefined();
  }));

  it('should add a product', function () {

    cartService.add(mockProduct1, 1);
    expect(cartService.cartItems.length).toEqual(1);
  });

  it('should not allow negative quantities to be placed in the cart', function() {
    cartService.add(mockProduct1, -1);
    expect(cartService.cartItems.length).toEqual(0);
  });

  it('should not allow trivial quantities to be placed in the cart', function() {
    cartService.add(mockProduct1, 0);
    expect(cartService.cartItems.length).toEqual(0);
  });

  it('should increment quantity and not add new item if the same product is added', function () {
    cartService.add(mockProduct1, 1);
    cartService.add(mockProduct1, 1);
    expect(cartService.cartItems.length).toEqual(1);
  });

  it('should return the correct price for single item with quantity more than 1', function() {
    cartService.add(mockProduct1, 1);
    cartService.add(mockProduct1, 1);
    var total = cartService.getTotal();
    expect(total).toEqual(mockProduct1.price * 2);
  });

  it('should return the correct price for multiple items', function() {
    cartService.add(mockProduct1, 1);
    cartService.add(mockProduct2, 1);
    var total = cartService.getTotal();
    expect(total).toEqual(mockProduct1.price + mockProduct2.price);
  });

  it('should add shipping to the cart total', function() {
    var selectedShippingOption = shippingService.shippingOptions[0];
    cartService.add(mockProduct1, 1);
    shippingService.selectedOption = selectedShippingOption;

    var total = cartService.getTotal();

    expect(total).toEqual(Number(selectedShippingOption.price) + Number(mockProduct1.price));
  });

  it('should remove the correct item from the cart', function() {
    cartService.add(mockProduct1, 1);
    cartService.add(mockProduct2, 1);
    cartService.remove(mockProduct1);
    expect(cartService.cartItems.length).toEqual(1);
    expect(cartService.cartItems[0].product).toEqual(mockProduct2);
  })
});