describe('Products service', function() {
  var productsService;
  var productsList;

  var mockProducts = [{
    id: 932036,
    name: "Walkman Car Connecting Pack for MD Walkman and CD Walkman (Model# CPA-9C)",
    price: 14.85,
    image: {
      small: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=100&odnWidth=100&odnBg=ffffff",
      medium: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=180&odnWidth=180&odnBg=ffffff",
      large: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=450&odnWidth=450&odnBg=ffffff"
    },
    shortDescription: "Listen to your favorite music while driving with the Sony CPA-9C CD To Cassette Adapter. This product is shaped like a cassette and is made to fit inside a tape deck. The Minidisc adapter is an ideal companion for daily commutes and road trips.",
    rating: 2.333
  },{
    id: 876589,
    name: "SONY Television",
    price: 15.37,
    image: {
      small: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=100&odnWidth=100&odnBg=ffffff",
      medium: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=180&odnWidth=180&odnBg=ffffff",
      large: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=450&odnWidth=450&odnBg=ffffff"
    },
    shortDescription: "Listen to your favorite music while driving with the Sony CPA-9C CD To Cassette Adapter. This product is shaped like a cassette and is made to fit inside a tape deck. The Minidisc adapter is an ideal companion for daily commutes and road trips.",
    rating: 4.2
  },{
    id: 876589,
    name: "SONY Television",
    price: 15.37,
    image: {
      small: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=100&odnWidth=100&odnBg=ffffff",
      medium: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=180&odnWidth=180&odnBg=ffffff",
      large: "http://i5.walmartimages.com/asr/57674f73-f84e-453a-9906-87a75964899f_1.67685c8f96235ef6562e2732e6de9840.jpeg?odnHeight=450&odnWidth=450&odnBg=ffffff"
    },
    shortDescription: "Listen to your favorite music while driving with the Sony CPA-9C CD To Cassette Adapter. This product is shaped like a cassette and is made to fit inside a tape deck. The Minidisc adapter is an ideal companion for daily commutes and road trips.",
    rating: 4.2
  }];

  beforeEach(module('HETest'));

  beforeEach(module(function($provide){
    $provide.constant('productsList', mockProducts);
  }));

  beforeEach(inject(function (_productsService_) {
    productsService = _productsService_;
    expect(productsService.getProducts()).toBeDefined();
  }));

  it('should return a paginated list of products', function() {
    var products = productsService.getProducts(1,1);
    expect(products.length).toBe(1);
  });

  it('should return a paginated list of products for the second page', function() {
    var products = productsService.getProducts(2,1);
    expect(products.length).toBe(1);
  });

  it('should return the correct product count', function () {
    var productCount = productsService.getProductCount();
    expect(productCount).toBe(mockProducts.length);
  })

});