(() => {
  'use strict';

  angular
    .module('HETest')
    .service('productsService', ProductsService);

  /* @ngInject */
  function ProductsService (productsList) {
    let self = this;

    self.getProducts = getProducts;
    self.getProductCount = getProductCount;


    function getProducts(page, pageCount) {
      let startSlice = (page - 1) * pageCount;
      let endSlice = startSlice + pageCount;
      return productsList.slice(startSlice, endSlice);
    }

    function getProductCount() {
      return productsList.length;
    }
  }
})();