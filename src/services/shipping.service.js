(() => {
  angular
    .module('HETest')
    .service('shippingService', ShippingService);

  function ShippingService() {
    let self = this;

    self.selectedShippingOption = {};

    self.shippingOptions = [{
      name: 'USPS',
      price: '10'
    },{
      name: 'FEDEX',
      price: '35'
    },{
      name: 'Overnight Express',
      price: '100'
    }];
  }
})();