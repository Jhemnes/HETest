(() => {
  'use strict';
  angular
    .module('HETest')
    .service('cartService', CartService);

  /* @ngInject */
  function CartService(shippingService) {
    let self = this;
    self.cartItems = [];
    self.add = add;
    self.getTotal = getTotal;
    self.getCartItems = getCartItems;
    self.remove = remove;

    function getTotal() {
      let cartTotal = self.cartItems.reduce((prev, curr) => {
        return prev + (curr.product.price * curr.quantity);
      }, Number(shippingService.selectedOption ? shippingService.selectedOption.price : 0));

      return cartTotal;
    }

    function getCartItems() {
      return self.cartItems;
    }

    function add(product, quantity) {
      if (quantity < 1) {
        return;
      }

      let productAlreadyInCart = self.cartItems.find((item) => {
        return item.product.id === product.id;
      });

      if (productAlreadyInCart) {
        productAlreadyInCart.quantity += Number(quantity);
      }
      else {
        self.cartItems.push({ product: angular.copy(product), quantity: angular.copy(quantity) })
      }
    }

    function remove(product) {
      self.cartItems = self.cartItems.filter((item) => {
        return item.product.id !== product.id;
      });
    }
  }
})();