(() => {
  angular
    .module('HETest')
    .component('product', {
      templateUrl: 'src/components/product/product.html',
      controller: ProductController,
      bindings: {
        product: '<'
      }
    });

  /* @ngInject */
  function ProductController(cartService) {
    let vm = this;
    vm.quantity = 1;

    vm.addToCart = addToCart;
    vm.canAddToCart = () => { return vm.quantity > 0 && Number.isInteger(vm.quantity)};


    function addToCart () {
      cartService.add(vm.product, vm.quantity)
    }
  }
})();