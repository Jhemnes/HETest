(() => {
  'use strict';
  angular
    .module('HETest')
    .component('productList', {
      templateUrl: 'src/components/product-list/product-list.html',
      controller: ProductListController,
      bindings: {}
    });
  /* @ngInject */
  function ProductListController(productsService) {
    let vm = this;
    vm.words = 'test';
    vm.page = 1;
    vm.pageCount = 4;
    vm.totalProductCount = productsService.getProductCount();
    vm.getProducts = getProducts;

    init();

    function init() {
      vm.getProducts();
    }

    function getProducts() {
      vm.products = productsService.getProducts(vm.page, vm.pageCount)
    }
  }
})();