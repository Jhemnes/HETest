(() => {
  'use strict';
  angular
    .module('HETest')
    .component('shippingOptions', {
      templateUrl: 'src/components/shipping/shipping.html',
      controller: ShippingOptionsController,
      bindings: {
        close: '&',
        dismiss: '&'
      }
    });
  /* @ngInject */
  function ShippingOptionsController(shippingService) {
    let vm = this;
    vm.selectedOption = angular.copy(shippingService.selectedOption);
    vm.shippingOptions = angular.copy(shippingService.shippingOptions);
  }
})();