(() => {
  angular
    .module('HETest')
    .component('cart', {
      templateUrl: 'src/components/cart/cart.html',
      controller: CartController,
      bindings: {}
    });

  /* @ngInject */
  function CartController(cartService, shippingService, $uibModal) {
    let vm = this;

    vm.getCartItems = cartService.getCartItems;
    vm.getTotal = cartService.getTotal;
    vm.remove = remove;
    vm.openShippingModal = openShippingModal;
    vm.getShippingPrice = getShippingPrice;
    vm.getShippingProvider = getShippingProvider;

    function remove(product) {
      cartService.remove(product);
    }

    function getShippingProvider() {
      return shippingService.selectedOption ? shippingService.selectedOption.name : '';
    }

    function getShippingPrice() {
      return shippingService.selectedOption ? shippingService.selectedOption.price : 0;
    }

    function openShippingModal() {
      let shippingModal = $uibModal.open({
        component: 'shippingOptions',
      });

      shippingModal.result.then((result) => {
        shippingService.selectedOption = result;
      });
    }


  }
})();