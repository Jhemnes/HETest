Project build dependencies
- `node v6 or greater`

To run this project:
- `npm start`

To run test suite
- `npm test`