const gulp = require('gulp');
const concat = require('gulp-concat');
const browserSync = require('browser-sync');
const browserSyncSpa = require('browser-sync-spa');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const ngAnnotate = require('gulp-ng-annotate');

browserSync.use(browserSyncSpa({
  selector: '[ng-app]'// Only needed for angular apps
}));

gulp.task('build:sass', function () {
  return gulp.src('./src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('./dist'));
});

gulp.task('build:js', () => {
  return gulp.src(['./src/app.js', './src/**/*.js', '!./src/**/*.spec.js'])
    .pipe(concat('bundle.js'))
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(ngAnnotate())
    .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['build:sass', 'build:js'], () => {
  return;
});

gulp.task('reload', ['build'], (done) => {
  browserSync.reload();
  done();
});

gulp.task('serve:watch', ['build'], () => {
  browserSync.init({
    server: {
      baseDir: './'
    }
  });

  gulp.watch('./src/**/*', ['reload']);
});

gulp.task('serve', () => {
  browserSync.init({
    server: {
      baseDir: './'
    }
  });
});
